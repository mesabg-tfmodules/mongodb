output "instance" {
  value       = module.mongo
  description = "Mongo instance created"
}
