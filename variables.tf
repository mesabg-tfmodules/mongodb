variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type = object({
    slug = string
    full = string
  })
  description = "General mongo name"
}

variable "instance_type" {
  type        = string
  description = "Instance size"
  default     = "t3a.small"
}

variable "key_name" {
  type        = string
  description = "Key name to access instance via SSH"
  default     = null
}

variable "security_groups" {
  type        = list(string)
  description = "Security Groups to be attached"
}

variable "subnet_id" {
  type        = string
  description = "VPC Subnet to locate the instance"
}

variable "associate_public_ip" {
  type        = bool
  description = "Associate a public IP or not"
  default     = false
}

variable "volume_size" {
  type        = number
  description = "Volume size"
  default     = 30
}

variable "namespace_id" {
  type        = string
  description = "CloudMap namespace identifier"
}
