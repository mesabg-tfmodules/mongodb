terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.13.0"
    }
  }
}

locals {
  ami = "ami-00f097cf92159a7b3" # Bitnami MongoDB AMI
}

module "mongo" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/instance.git?ref=v1.0.0"

  environment         = var.environment
  name                = var.name.full

  ami                 = local.ami
  instance_type       = var.instance_type
  key_name            = var.key_name
  security_groups     = var.security_groups
  subnet_id           = var.subnet_id
  associate_public_ip = var.associate_public_ip
  volume_size         = var.volume_size
}

module "discovery_instance" {
  source        = "git::https://gitlab.com/mesabg-tfmodules/discovery-instance.git?ref=v1.0.0"

  name          = var.name

  namespace_id  = var.namespace_id
  instance_id   = module.mongo.id
  ip            = module.mongo.private_ip
}
