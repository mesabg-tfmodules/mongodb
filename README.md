# Mongo Module

This module is capable to create a stable MongoDB instance

Module Input Variables
----------------------

- `name` - general name
- `namespace_id` - namespace identifier
- `instance_id` - instance identifier
- `ip` - ip address to register

Usage
-----

```hcl
module "mongo" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/mongodb.git"

  environment         = "develop"
  name = {
    slug              = "name"
    full              = "Full_Name"
  }

  instance_type       = "t3a.small"
  key_name            = "key_name"
  security_groups     = ["sg-xxxx"]
  subnet_id           = "subnet-xxxx"
  associate_public_ip = true
  namespace_id        = "ns-xxxx"
}

module "mongo" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/mongodb.git"

  environment         = "develop"
  name = {
    slug              = "name"
    full              = "Full_Name"
  }

  key_name            = "key_name"
  security_groups     = ["sg-xxxx"]
  subnet_id           = "subnet-xxxx"
  namespace_id        = "ns-xxxx"
}
```

Outputs

 - `instance` - Mongo instance created

Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
